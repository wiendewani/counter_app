import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  Widget radiusBar() {
    return CircularPercentIndicator(
      radius: 150.0,
      lineWidth: 20.0,
      animation: true,
      circularStrokeCap: CircularStrokeCap.round,
      percent: 1,
      progressColor: const Color(0xff3DB6AA),
      backgroundColor: const Color(0xff1E1E1E),
      center: Obx(
        () => Text(
          '${controller.allCount}',
          style: const TextStyle(fontSize: 75, color: Colors.white),
        ),
      ),
    );
  }

  Widget fieldButton(String title, VoidCallback onPressed) {
    return SizedBox(
      width: 100,
      height: 50,
      child: TextButton(
        style: TextButton.styleFrom(
          primary: Colors.white,
          textStyle: const TextStyle(fontSize: 20),
          side: const BorderSide(color: Color(0xff3DB6AA), width: 2),
        ),
        onPressed: () => onPressed(),
        child: Text(
          title,
          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
      ),
    );
  }

  PreferredSizeWidget appBar() {
    return PreferredSize(
      preferredSize: const Size.fromHeight(60),
      child: AppBar(
        backgroundColor: Colors.black,
        title: const Text('Counter'),
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {
            controller.restart();
          },
          child: const Icon(
            Icons.replay, // add custom icons also
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: appBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(child: radiusBar()),
          const SizedBox(
            height: 80,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              fieldButton("-", () => controller.decrement()),
              fieldButton("+", () => controller.increment()),
            ],
          )
        ],
      ),
    );
  }
}
