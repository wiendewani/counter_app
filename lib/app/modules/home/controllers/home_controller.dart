import 'package:get/get.dart';

class HomeController extends GetxController {

  final allCount = 0.obs;
  final _roundCount = 0.obs;
  final percent = 0.obs;
  final onTap =  false.obs;


  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void increment() {
    onTap.value = true;
    allCount.value++;
    _roundCount.value++;
    percent.value = _roundCount.value *  0.01.toInt();
    if (_roundCount.value == 33) {
      _roundCount.value = 0;
    }
  }

  void decrement() {
    onTap.value = true;
    allCount.value++;
    _roundCount.value++;
    percent.value = _roundCount.value *  0.01.toInt();
    if (_roundCount.value == 33) {
      _roundCount.value = 0;
    }
  }
  void restart(){
    allCount.value=0;
  }

}
